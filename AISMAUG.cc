#include "Player.hh"
#define PLAYER_NAME SMAUG


struct PLAYER_NAME : public Player {
    static Player* factory(){
        return new PLAYER_NAME;
    }
    
    struct Tot{
        bool v; //Visitat true o false
        int d; //Distancia actual al node
        Pos a; //"Pare" del node
        Dir m; //Moviment per arribar desde el pare
    };
    
    typedef pair<int, Pos> Aresta;
    typedef vector<int> VI;
    typedef vector<Tot> VTot;
    typedef vector<VTot> MTot;
    typedef set<Pos> SPos;

    Dir busca_enanos_magos(const Pos& pos_inicial, const SPos& prohibidas, int target, int dist_max){ //target = 0 -> Enano (menos vida -25), target = 1 -> Mago
        Tot init = {false,-1,Pos(-1,-1),None};
        MTot M(60,VTot(60,init));
        M[pos_inicial.i][pos_inicial.j].d = 0;
        priority_queue<Aresta,vector<Aresta>,greater<Aresta>> Q;
        Q.push(Aresta(0, pos_inicial));
        UnitType U;
        if (target == 0) U = Dwarf;
        else U = Wizard;
        while (not Q.empty()){
            if (Q.top().first >= dist_max) return None;
            Pos pos_actual = Q.top().second; Q.pop();
            int id = cell(pos_actual).id;
            if (id != -1 and unit(id).type == U and unit(id).player != me()){
                if (target == 0){
                    if (unit(id).health - 25 <= unit(cell(pos_inicial).id).health){
                        Pos aux = pos_actual;
                        while (M[aux.i][aux.j].a != pos_inicial) aux = M[aux.i][aux.j].a;
                        return M[aux.i][aux.j].m;
                    }
                }
                else{
                    Pos aux = pos_actual;
                    while (M[aux.i][aux.j].a != pos_inicial) aux = M[aux.i][aux.j].a;
                    return M[aux.i][aux.j].m;
                }
            }
            if (not M[pos_actual.i][pos_actual.j].v){
                M[pos_actual.i][pos_actual.j].v = true;
                for (int i = 0; i < 8; ++i){
                    Pos ad = pos_actual + Dir(i);
                    if (pos_ok(ad) and not M[ad.i][ad.j].v){
                        if(not posicio_prohibida(ad, prohibidas)){
                            if (cell(ad).type != Granite and cell(ad).type != Abyss){
                                int id_ad = cell(ad).id;
                                if (id_ad != -1){
                                    if (unit(id_ad).player != me() and unit(id_ad).player != -1){
                                        int pes = 1;
                                        if (cell(ad).type == Rock) pes = cell(ad).turns + 1;
                                        if (M[ad.i][ad.j].d == -1 or (M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + pes)){
                                            M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + pes;
                                            M[ad.i][ad.j].a = pos_actual;
                                            M[ad.i][ad.j].m = Dir(i);
                                            Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                        }
                                    }
                                }
                                else{
                                    int pes = 1;
                                    if (cell(ad).type == Rock) pes = cell(ad).turns +1;
                                    if (M[ad.i][ad.j].d == -1 or (M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + pes)){
                                        M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + pes;
                                        M[ad.i][ad.j].a = pos_actual;
                                        M[ad.i][ad.j].m = Dir(i);
                                        Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return None;
    }

    Dir busca_tesoros(const Pos& pos_inicial, const SPos& prohibidas, int dist_max){
        Tot init = {false,-1,Pos(-1,-1),None};
        MTot M(60,VTot(60,init));
        M[pos_inicial.i][pos_inicial.j].d = 0;
        priority_queue<Aresta,vector<Aresta>,greater<Aresta>> Q;
        Q.push(Aresta(0, pos_inicial));
        while(not Q.empty()){
            if (Q.top().first >= dist_max) return None;
            Pos pos_actual = Q.top().second; 
            Q.pop();
            if (cell(pos_actual).treasure){
                Pos aux = pos_actual;
                while(M[aux.i][aux.j].a != pos_inicial) aux = M[aux.i][aux.j].a;
                return M[aux.i][aux.j].m;
            }
            if (not M[pos_actual.i][pos_actual.j].v){
                M[pos_actual.i][pos_actual.j].v = true;
                for (int i = 0; i < 8; ++i){
                    Pos ad = pos_actual + Dir(i);
                    if (pos_ok(ad) and not M[ad.i][ad.j].v){
                        if(not posicio_prohibida(ad, prohibidas)){
                            if (cell(ad).type != Granite and cell(ad).type != Abyss){
                                int id_ad = cell(ad).id;
                                if (id_ad != -1){
                                    if (unit(id_ad).player != me() and unit(id_ad).player != -1){
                                        int pes = 1;
                                        if (cell(ad).type == Rock) pes = cell(ad).turns + 1;
                                        if (M[ad.i][ad.j].d == -1 or (M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + pes)){
                                            M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + pes;
                                            M[ad.i][ad.j].a = pos_actual;
                                            M[ad.i][ad.j].m = Dir(i);
                                            Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                        }
                                    }
                                }
                                else{
                                    int pes = 1;
                                    if (cell(ad).type == Rock) pes = cell(ad).turns +1;
                                    if (M[ad.i][ad.j].d == -1 or (M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + pes)){
                                        M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + pes;
                                        M[ad.i][ad.j].a = pos_actual;
                                        M[ad.i][ad.j].m = Dir(i);
                                        Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return None;
    }

    Dir conquer(const Pos& pos_inicial, const SPos& prohibidas){
        Tot init = {false,-1,Pos(-1,-1),None};
        MTot M(60,VTot(60,init));
        M[pos_inicial.i][pos_inicial.j].d = 0;
        priority_queue<Aresta,vector<Aresta>,greater<Aresta>> Q;
        Q.push(Aresta(0, pos_inicial));
        while (not Q.empty()){
            Pos pos_actual = Q.top().second; 
            Q.pop();
            if (cell(pos_actual).owner != me() and pos_actual != pos_inicial and cell(pos_actual).type != Outside){
                Pos aux = pos_actual;
                while (M[aux.i][aux.j].a != pos_inicial) aux = M[aux.i][aux.j].a;
                return M[aux.i][aux.j].m;
            }
            if (not M[pos_actual.i][pos_actual.j].v){
                M[pos_actual.i][pos_actual.j].v = true;
                for (int i = 0; i < 8; ++i){
                    Pos ad = pos_actual + Dir(i);
                    if (pos_ok(ad) and not M[ad.i][ad.j].v){
                        if(not posicio_prohibida(ad, prohibidas)){
                            if (cell(ad).type != Granite and cell(ad).type != Abyss){
                                int id_ad = cell(ad).id;
                                if (id_ad != -1){
                                    if (unit(id_ad).player != me() and unit(id_ad).player != -1){
                                        int pes = 1;
                                        if (cell(ad).type == Outside) pes = 20;
                                        if (cell(ad).type == Rock) pes = cell(ad).turns + 1;
                                        if (M[ad.i][ad.j].d == -1 or (M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + pes)){
                                            M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + pes;
                                            M[ad.i][ad.j].a = pos_actual;
                                            M[ad.i][ad.j].m = Dir(i);
                                            Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                        }   
                                    }
                                }
                                else{
                                int pes = 1;
                                    if (cell(ad).type == Outside) pes = 20;
                                    if (cell(ad).type == Rock) pes = cell(ad).turns + 1;
                                    if (M[ad.i][ad.j].d == -1 or (M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + pes)){
                                        M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + pes;
                                        M[ad.i][ad.j].a = pos_actual;
                                        M[ad.i][ad.j].m = Dir(i);
                                        Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                    }  
                                }
                            }
                        }
                    }
                }
            }
        }
        return None;
    }

    Dir huye(const Pos& pos_inicial, const SPos& prohibidas, int tropa){ //tropa = 0 -> Enano, tropa = 1 -> Mago
        Tot init = {false,-1,Pos(-1,-1),None};
        MTot M(60,VTot(60,init));
        M[pos_inicial.i][pos_inicial.j].d = 0;;
        priority_queue<Aresta,vector<Aresta>,greater<Aresta>> Q;
        Q.push(Aresta(0, pos_inicial));
        while(not Q.empty()){
            Pos pos_actual = Q.top().second; 
            Q.pop();
            if (cell(pos_actual).type == Outside and pos_actual != pos_inicial){
                Pos aux = pos_actual;
                while (M[aux.i][aux.j].a != pos_inicial) aux = M[aux.i][aux.j].a;
                return (M[aux.i][aux.j].m);
            }
            if (not M[pos_actual.i][pos_actual.j].v){
                M[pos_actual.i][pos_actual.j].v = true;
                int caselles;
                if (tropa == 1) caselles = 4;
                else caselles = 8;
                for (int i = 0; i < caselles; ++i){
                    Pos ad;
                    if (tropa == 1) ad = pos_actual + Dir(2*i);
                    else ad = pos_actual + Dir(i);
                    if (pos_ok(ad) and not M[ad.i][ad.j].v){
                        if(not posicio_prohibida(ad, prohibidas)){
                            if (cell(ad).type != Granite and cell(ad).type != Abyss and cell(ad).type != Rock){
                                if (cell(ad).id == -1){
                                    if (M[ad.i][ad.j].d == -1 or (M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + 1)){
                                        M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + 1;
                                        M[ad.i][ad.j].a = pos_actual;
                                        if (tropa == 1) M[ad.i][ad.j].m = Dir(2*i);
                                        else M[ad.i][ad.j].m = Dir(i);
                                        Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return None;
    }

    Dir magos_busca_enanos(const Pos& pos_inicial, const SPos& prohibidas, int dist_max){
        Tot init = {false,-1,Pos(-1,-1),None};
        MTot M(60,VTot(60,init));
        M[pos_inicial.i][pos_inicial.j].d = 0;
        priority_queue<Aresta,vector<Aresta>,greater<Aresta>> Q;
        Q.push(Aresta(0, pos_inicial));
        Dir mov_health = None;
        Dir mov = None;
        int dist_health = -1;
        int dist = -1;
        while(not Q.empty()){
            if (Q.top().first >= dist_max){
                if (dist_health!=-1){
                    if (dist!=-1){
                        if (dist_health < dist+2) return mov_health;
                        return mov;
                    }
                    return mov_health;
                }
                if (dist!=-1) return mov;
                return None;
            }
            Pos pos_actual = Q.top().second; 
            Q.pop();
            int id = cell(pos_actual).id;
            if (id != -1 and unit(id).player == me() and unit(id).type == Dwarf){
                if (unit(id).health <= 70 and (dist_health==-1 or (M[pos_actual.i][pos_actual.j].d < dist_health))){
                    Pos aux = pos_actual;
                    dist_health = M[aux.i][aux.j].d;
                    while(M[aux.i][aux.j].a != pos_inicial) aux = M[aux.i][aux.j].a;
                    mov_health = (M[aux.i][aux.j].m);
                }
                else if (dist == -1 or M[pos_actual.i][pos_actual.j].d < dist){
                    Pos aux = pos_actual;
                    dist = M[aux.i][aux.j].d;
                    while(M[aux.i][aux.j].a != pos_inicial) aux = M[aux.i][aux.j].a;
                    mov = (M[aux.i][aux.j].m);
                }
            }
            if (not M[pos_actual.i][pos_actual.j].v){
                M[pos_actual.i][pos_actual.j].v = true;
                for (int i = 0; i < 4; ++i){
                    Pos ad = pos_actual + Dir(i*2);
                    if (pos_ok(ad) and not M[ad.i][ad.j].v){
                        if(not posicio_prohibida(ad, prohibidas)){
                            if (cell(ad).type != Granite and cell(ad).type != Abyss and cell(ad).type != Rock){
                                int id_ad = cell(ad).id;
                                if (id_ad != -1){
                                    if (unit(id_ad).player == me()){
                                        if (M[ad.i][ad.j].d  == -1 or (M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + 1)){
                                            M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + 1;
                                            M[ad.i][ad.j].a = pos_actual;
                                            M[ad.i][ad.j].m = Dir(i*2);
                                            Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                        }
                                    }
                                }
                                else{
                                    if (M[ad.i][ad.j].d  == -1 or M[ad.i][ad.j].d > M[pos_actual.i][pos_actual.j].d + 1){
                                        M[ad.i][ad.j].d = M[pos_actual.i][pos_actual.j].d + 1;
                                        M[ad.i][ad.j].a = pos_actual;
                                        M[ad.i][ad.j].m = Dir(i*2);
                                        Q.push(Aresta(M[ad.i][ad.j].d, ad));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (dist_health!=-1){
            if (dist!=-1){
                if (dist_health < dist+2) return mov_health;
                return mov;
            }
            return mov_health;
        }
        if (dist!=-1) return mov;
        return None;
    }

    Dir move_safe(const Pos& pos_inicial, const SPos& prohibidas){
        for (int i = 0; i < 4; ++i){
            Pos pos_actual = pos_inicial+Dir(2*i);
            if (pos_ok(pos_actual) and not posicio_prohibida(pos_actual,prohibidas)){
                if (not amenazada(pos_actual) and cell(pos_actual).id == -1 and (cell(pos_actual).type == Outside or cell(pos_actual).type == Cave)) return Dir(2*i);
            }
        }
        return None;
    }
    
    Dir ataca(const Pos& pos_inicial, const SPos& prohibidas){
        int minima_vida = 500;
        int ataca_dir = 8;
        for (int i = 0; i < 8; ++i){
            Pos pos_actual = pos_inicial+Dir(i);
            if (pos_ok(pos_actual) and not posicio_prohibida(pos_actual,prohibidas) and cell(pos_actual).id != -1 and unit(cell(pos_actual).id).player != me()){
                if (minima_vida > unit(cell(pos_actual).id).health){
                    minima_vida = unit(cell(pos_actual).id).health;
                    ataca_dir = i;
                }
            }
        }
        return Dir(ataca_dir);
    }

    Dir last_chance(const Pos& pos_inicial, const SPos& prohibidas, int tropa){
        int caselles;
        if (tropa == 0) caselles = 8;
        else caselles = 4;
        for (int i = 0; i < caselles; ++i){
            Pos pos_actual;
            if (tropa == 1) pos_actual = pos_inicial + Dir(2*i);
            else pos_actual = pos_inicial + Dir(i);
            if (pos_ok(pos_actual) and not posicio_prohibida(pos_actual,prohibidas) and cell(pos_actual).id == -1 and (cell(pos_actual).type == Outside or cell(pos_actual).type == Cave)){
                if (tropa == 0) return Dir(i);
                return Dir(2*i);
            }
        }
        return None;
    }
    
    void find_balrog_trolls(SPos& prohibidas){
        Pos pos_inicial = Pos(0,0);
        for (int i = 0; i < 60; ++i){
            for (int j = 0; j < 60; ++j){
                Pos aux = pos_inicial+Pos(i,j);
                int id = cell(aux).id;
                if (id != -1 and unit(id).player == -1 and unit(id).type != Orc){
                    Pos aux2 = aux;
                    prohibidas.insert(aux2);
                    for (int i = 0; i < 8; ++i){
                        aux2 = aux+Dir(i);
                        prohibidas.insert(aux2);
                    }
                    if (id == balrog_id()){
                        aux2 = (aux+TL)+TL;
                        for(int i = 0; i < 3; ++i){
                            aux2 = aux2 + Right;
                            prohibidas.insert(aux2);
                            if (i == 1) prohibidas.insert(aux2+Top);
                        }
                        aux2 = aux2 + Right;
                        for(int i = 0; i < 3; ++i){
                            aux2 = aux2 + Bottom;
                            prohibidas.insert(aux2);
                            if (i == 1) prohibidas.insert(aux2+Right);
                        }
                        aux2 = aux2 + Bottom;
                        for(int i = 0; i < 3; ++i){
                            aux2 = aux2 + Left;
                            prohibidas.insert(aux2);
                            if (i == 1) prohibidas.insert(aux2+Bottom);
                        }
                        aux2 = aux2 + Left;
                        for(int i = 0; i < 3; ++i){
                            aux2 = aux2 + Top;
                            prohibidas.insert(aux2);
                            if (i == 1) prohibidas.insert(aux2+Left);
                        }
                    }
                }
            }
        }
    }

    bool posicio_prohibida(const Pos& posicio, const SPos& prohibidas){
        set<Pos>::const_iterator it = prohibidas.find(posicio);
        return it != prohibidas.end();
    }

    bool amenazada(const Pos& pos_inicial){ //posicio amenazada si = true, no = false
        for (int i = 0; i < 8; ++i){
            Pos pos_actual = pos_inicial + Dir(i);
            if (pos_ok(pos_actual)){
                int id = cell(pos_actual).id;
                if (id != -1 and unit(id).player != me() and unit(id).type != Wizard) return true;
            }
        }
        return false;
    }
    
    void move_dwarves(const SPos& prohibidas){
        VI D = dwarves(me());
        int n = D.size();
        VI perm1 = random_permutation(n);
        for (int i = 0; i < n; ++i){
            int id = D[perm1[i]];
            Pos pos_inicial = unit(id).pos;
            if (posicio_prohibida(pos_inicial, prohibidas)){
                Dir dir_huir = huye(pos_inicial, prohibidas, 0);
                if (dir_huir != None and cell(pos_inicial+dir_huir).id == -1) command(id, dir_huir);
                else command(id, last_chance(pos_inicial,prohibidas,0));
            }
            else{
                Dir dir_ataque = ataca(pos_inicial, prohibidas);
                if (dir_ataque != None) command(id, dir_ataque);
                else{
                    Dir dir_mago = busca_enanos_magos(pos_inicial, prohibidas, 1, 7);
                    if (dir_mago != None) command(id, dir_mago);
                    else{
                        Dir dir_enano = busca_enanos_magos(pos_inicial, prohibidas, 0, 5);
                        if (dir_enano != None) command(id, dir_enano);
                        else{
                            Dir dir_tesoro = busca_tesoros(pos_inicial, prohibidas, 20);
                            if (dir_tesoro != None) command(id, dir_tesoro);
                            else{
                                Dir dir_conquer = conquer(pos_inicial, prohibidas);
                                command(id, dir_conquer);
                            }
                        }
                    }
                }
            }
        }
    }

    void move_wizards(const SPos& prohibidas){
        VI W = wizards(me());
        int m = W.size();
        VI perm2 = random_permutation(m);
        for (int i = 0; i < m; ++i){
            int id = W[perm2[i]];
            Pos pos_inicial = unit(id).pos;
            if (posicio_prohibida(pos_inicial, prohibidas)){
                Dir dir_huir = huye(pos_inicial, prohibidas, 1);
                if (dir_huir != None and cell(pos_inicial+dir_huir).id == -1) command(id, dir_huir);
                else command(id, last_chance(pos_inicial,prohibidas,1));
            }
            else{
                Dir dir_safe = None;
                if (amenazada(pos_inicial)){
                    dir_safe = move_safe(pos_inicial,prohibidas);
                    if (dir_safe != None) command(id, dir_safe);
                }
                if (dir_safe == None){
                    Dir dir_magos_buscando = magos_busca_enanos(pos_inicial, prohibidas, 50);
                    command(id, dir_magos_buscando);
                }
            }
        }
    }

    virtual void play(){
        set<Pos> prohibidas;
        find_balrog_trolls(prohibidas);
        move_dwarves(prohibidas);
        move_wizards(prohibidas);
    }
};

RegisterPlayer(PLAYER_NAME);
