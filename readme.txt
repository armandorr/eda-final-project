
----IN FOLDER game----

First of all:

	1) Copy one of the objects:

	 AIDummy.o.Linux64 (Linux   64 bits)
	 AIDummy.o.Linux32 (Linux   32 bits)
	 AIDummy.o.Win32   (Windows 32 bits)
	 AIDummy.o.Win64   (Windows 64 bits)
 	 AIDummy.o.MacOS   (Mac)
 
 	 to AIDummy.o
	
	 (p.e.) cp AIDummy.o.Linux64 AIDummy.o                                                                                                                        

                         
	2) Copy one of the objects:

 	 Board.o.Linux64 (Linux   64 bits)
	 Board.o.Linux32 (Linux   32 bits)
 	 Board.o.Win32   (Windows 32 bits)
 	 Board.o.Win64   (Windows 64 bits)
	 Board.o.MacOS   (Mac)

 	 to Board.o

	 (p.e.) cp Board.o.Linux64 Board.o

Compile:
	# make all

Execute:
	# ./Game SMAUG Dummy Dummy Dummy -s 45 -i default.cnf -o default.res 
	
	This starts a match with my IA and 3 dummys, with random seed 45 in the board defined in default.cnf. 
	The output of this match is redirected to default.res.